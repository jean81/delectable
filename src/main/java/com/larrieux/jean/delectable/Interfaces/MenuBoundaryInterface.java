package com.larrieux.jean.delectable.Interfaces;



import com.larrieux.jean.delectable.Entities.MenuItem;

import java.util.List;



/**
 * Created by Jeannius on 4/12/2016.
 */
public interface MenuBoundaryInterface {


    void add(MenuItem item);

    MenuItem getMenuItemByID(int id);

    List<MenuItem> getFullMenu();
}
