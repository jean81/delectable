package com.larrieux.jean.delectable.Interfaces;



import com.larrieux.jean.delectable.Entities.Customer;

import java.util.ArrayList;



/**
 * Created by Jeannius on 4/17/2016.
 */
public interface CustomerBoundaryInterface {


    void add(Customer customer, int OrderID);

    boolean doesCustomerExist(Customer joe);

    Customer getCustomerByID(int ID);

    ArrayList<Customer> getAllCustomers();


    Customer findByName(String name);
}
