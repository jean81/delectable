package com.larrieux.jean.delectable.Interfaces;



import com.larrieux.jean.delectable.Entities.Order;

import java.util.ArrayList;
import java.util.Date;



/**
 * Created by Jlarrieux on 4/14/2016.
 */
public interface OrderBoundaryInterface {


    String CANCEL = "cancel";
    String DELIVERED = "delivered";

    void add(int menuItemID, int count, Order order);

    ArrayList<Order> getOrderBook();

    Order getOrderByID(int oid);

    double getSurcharge();

    void setSurcharge(double value);

    void submitOrder(Order order);

    void setDeliveryDate(Date deliveryDate, Order order);


}
