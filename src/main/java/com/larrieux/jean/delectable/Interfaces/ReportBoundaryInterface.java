package com.larrieux.jean.delectable.Interfaces;



import com.larrieux.jean.delectable.Entities.DeliveryReport;
import com.larrieux.jean.delectable.Entities.Report;
import com.larrieux.jean.delectable.Entities.RevenueReport;

import java.util.ArrayList;



/**
 * Created by Jlarrieux on 4/19/2016.
 */
public interface ReportBoundaryInterface {


    ArrayList<Report> getAllDeliveryReports();

    DeliveryReport getDeliveryReport(int reportID, String startDate, String endDate);

    RevenueReport getRevenueReport(String startDate, String endDate);

    int[] getReportIds();

}
