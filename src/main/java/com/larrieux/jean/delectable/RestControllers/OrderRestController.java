package com.larrieux.jean.delectable.RestControllers;



import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.larrieux.jean.delectable.Entities.Customer;
import com.larrieux.jean.delectable.Entities.CustomerContactInfo;
import com.larrieux.jean.delectable.Entities.Order;
import com.larrieux.jean.delectable.Interfaces.CustomerBoundaryInterface;
import com.larrieux.jean.delectable.Interfaces.OrderBoundaryInterface;
import com.larrieux.jean.delectable.Managers.CustomerManager;
import com.larrieux.jean.delectable.Managers.OrderManager;
import com.larrieux.jean.delectable.Util;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.Calendar;
import java.util.Date;



/**
 * Created by Jlarrieux on 4/14/2016.
 */

@Path("/order")
public class OrderRestController extends AbstractController {


    OrderBoundaryInterface orderBoundaryInterface = OrderManager.getInstance();

    CustomerBoundaryInterface customerBoundaryInterface = CustomerManager.getInstance();



    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllOrders(@DefaultValue("") @QueryParam(Util.DATE) String date) {
        Calendar cal1 = Calendar.getInstance();
        Calendar delivery = Calendar.getInstance();

        JsonArray array = new JsonArray();
        for (Order order : orderBoundaryInterface.getOrderBook()) {
            if(order.getStatus()!= OrderBoundaryInterface.CANCEL){
                if (date.length() == 0) array.add(buildOrderForJson(order));
                else {
                    delivery.setTime(Util.getDateFromString(date));
                    cal1.setTime(order.getDeliveryDate());
                    if (areDatesEquals(delivery.getTime(), cal1.getTime())) array.add(buildOrderForJson(order));
                }
            }



        }

        return buildResponseStatusOk200(array);
    }



    private boolean areDatesEquals(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(date1);
        cal2.setTime(date2);
        Boolean dateEqual, monthEqual, yearEqual;
        dateEqual = cal1.get(Calendar.DATE) == cal2.get(Calendar.DATE);
        monthEqual = cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH);
        yearEqual = cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR);


        return dateEqual && monthEqual && yearEqual;
    }



    private JsonObject buildOrderForJson(Order order) {
        JsonObject result = new JsonObject();
        result.addProperty(Util.ID, order.getOrderID());
        result.addProperty(Util.ORDER_DATE, DateToYYYYMMDDformat(order.getOrderDate()));
        result.addProperty(Util.DELIVERY_DATE, DateToYYYYMMDDformat(order.getDeliveryDate()));
        result.addProperty(Util.AMOUNT, order.getTotalPrice());
        result.addProperty(Util.SURCHAGE, order.getSurcharge());
        result.addProperty(Util.STATUS, order.getStatus());
        Customer customer = customerBoundaryInterface.getCustomerByID(order.getCustomerID());
        if(customer!=null) result.addProperty(Util.ORDERED_BY, customer.getEmail());
        return result;
    }



    private String DateToYYYYMMDDformat(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);


        String result = String.valueOf(cal.get(Calendar.YEAR));
        result += padding(String.valueOf(cal.get(Calendar.MONTH)));
        result += padding(String.valueOf(cal.get(Calendar.DATE)));

        return result;
    }



    private String padding(String s) {
        String result = "0";
        if (s.length() < 2) {
            result += s;
            return result;
        } else return s;
    }



    @Path("{oid}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getOrderByID(@PathParam("oid") int oid) {

        Order order = orderBoundaryInterface.getOrderByID(oid);
        if (order == null) return buildResponseStatusNotFound404(oid, Order.class.getSimpleName());
        else return buildResponseStatusOk200(order);

    }



    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response createOrder(String json, @Context UriInfo uriInfo) {

        JsonObject jsonObject = getJsonObjectFromJsonString(json);
        Date deliveryDate = Util.getDateFromString(getStringFromJsonObject(jsonObject, Util.DELIVERY_DATE));
        String deliveryAddress = getStringFromJsonObject(jsonObject, Util.DELIVERY_ADDRESS);
        JsonObject cus = jsonObject.getAsJsonObject(Util.PERSONAL_INFO);

        Customer customer = getCustomerFromJsonObject(cus);
        if(customerBoundaryInterface.doesCustomerExist(customer))customer = customerBoundaryInterface.findByName(customer.getCustomerName());

        String note = getStringFromJsonObject(jsonObject, Util.NOTE);


        Order order = new Order();
        try{
            addItemsToOrder(jsonObject.getAsJsonArray(Util.ORDER_DETAIL), order);
            order.setCustomerID(customer.getCustomerID());
            order.setDeliveryAddress(deliveryAddress);
            orderBoundaryInterface.setDeliveryDate(deliveryDate, order);
        }
        catch (OrderManager.MinimumNumberOfPeopleNotMetForThisItem e){
            String message = "Minimum order requirement not met";
            return buildResponseBadReques400(message);
        }catch (OrderManager.itemNotFound err){
            String message = "Menu item not found " ;
            return buildResponseBadReques400(message);
        }catch (OrderManager.deliveryDateIsBeforeToday d){
            String message = "Delivery date is before today.";
            return buildResponseBadReques400(message);
        }

        order.setNote(note);



        customerBoundaryInterface.add(customer, order.getOrderID());
        orderBoundaryInterface.submitOrder(order);
        JsonObject object = new JsonObject();
        object.addProperty(Util.ID, order.getOrderID());
        object.addProperty(Util.CANCEL_URL, Util.ORDER_CANCEL+ String.valueOf(order.getOrderID()));
        UriBuilder builder = uriInfo.getAbsolutePathBuilder();
        builder.path(Integer.toString(order.getOrderID()));
        return buildResponseCreated201(object, builder);
    }



    private Customer getCustomerFromJsonObject(JsonObject jsonObject) {
        String name = getStringFromJsonObject(jsonObject, Util.NAME).toLowerCase();
        String email = getStringFromJsonObject(jsonObject, Util.EMAIL).toLowerCase();
        String phone = getStringFromJsonObject(jsonObject, Util.PHONE);

        return new Customer(name, new CustomerContactInfo(email, phone));
    }


    private void addItemsToOrder(JsonArray jsonArray, Order order) {

        for (int i = 0; i < jsonArray.size(); i++) {
            JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
            int id = getIntFromJsonObject(jsonObject, Util.ID);
            int count = getIntFromJsonObject(jsonObject, Util.COUNT);
            orderBoundaryInterface.add(id, count, order);

        }
    }



    @Path("/cancel/{oid}")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response cancelOrderByID(@PathParam("oid") int oid) {
        Order order = orderBoundaryInterface.getOrderByID(oid);
        if (order == null) return buildResponseStatusNotFound404(oid, Order.class.getSimpleName());
        else {
            Calendar today = Calendar.getInstance();
            if(Util.isSameDate(order.getDeliveryDate(), today.getTime())){
                return buildResponseBadReques400(Util.CANNOT_CANCEL_ORDER_DUE_TODAY);
            }
            else order.setStatus(OrderBoundaryInterface.CANCEL);
            return buildResponseNoContent204();
        }
    }


}
