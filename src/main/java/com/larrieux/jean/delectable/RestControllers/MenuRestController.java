package com.larrieux.jean.delectable.RestControllers;



import com.larrieux.jean.delectable.Entities.MenuItem;
import com.larrieux.jean.delectable.Interfaces.MenuBoundaryInterface;
import com.larrieux.jean.delectable.Managers.MenuItemManager;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;



/**
 * Created by Jeannius on 4/12/2016.
 */
@Path("/menu")
public class MenuRestController extends AbstractController {


    private MenuBoundaryInterface menuInterface = MenuItemManager.getInstance();



    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getFullMenu() {
        return buildResponseStatusOk200(menuInterface.getFullMenu());
    }



    @Path("{mid}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMenuItemByID(@PathParam("mid") int mID) {

        MenuItem item = menuInterface.getMenuItemByID(mID);
        if (item == null) return buildResponseStatusNotFound404(mID, MenuItem.class.getSimpleName());
        else   return buildResponseStatusOk200(item);

    }


}
