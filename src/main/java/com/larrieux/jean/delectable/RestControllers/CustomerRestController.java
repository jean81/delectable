package com.larrieux.jean.delectable.RestControllers;



import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.larrieux.jean.delectable.Entities.Customer;
import com.larrieux.jean.delectable.Entities.Order;
import com.larrieux.jean.delectable.Interfaces.CustomerBoundaryInterface;
import com.larrieux.jean.delectable.Interfaces.OrderBoundaryInterface;
import com.larrieux.jean.delectable.Managers.CustomerManager;
import com.larrieux.jean.delectable.Managers.OrderManager;
import com.larrieux.jean.delectable.Util;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Calendar;



/**
 * Created by Jeannius on 4/17/2016.
 */

@Path("/customer")
public class CustomerRestController extends AbstractController {


    CustomerBoundaryInterface customerBoundaryInterface = CustomerManager.getInstance();
    OrderBoundaryInterface orderBoundaryInterface = OrderManager.getInstance();


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllCustomers(@DefaultValue("") @QueryParam(Util.KEY) String key) {
        JsonArray array = new JsonArray();
        for (Customer customer : customerBoundaryInterface.getAllCustomers()) {
            if (key.length() == 0) array.add(buildCustomerForJson(customer));
            else {
                key = key.toLowerCase();
                if (customer.getCustomerName().contains(key) || customer.getEmail().contains(key) || customer.getPhone().contains(key))
                    array.add(buildCustomerForJson(customer));
            }
        }

        return buildResponseStatusOk200(array);
    }



    private JsonObject buildCustomerForJson(Customer customer) {

        JsonObject result = new JsonObject();
        result.addProperty(Util.ID, customer.getCustomerID());
        result.addProperty(Util.NAME, customer.getCustomerName());
        result.addProperty(Util.EMAIL, customer.getEmail());
        result.addProperty(Util.PHONE, customer.getPhone());

        return result;

    }



    @GET
    @Path("{cid}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomerByID(@PathParam("cid") int cid) {

        Customer customer = customerBoundaryInterface.getCustomerByID(cid);
        if (customer == null) return buildResponseStatusNotFound404(cid, Customer.class.getSimpleName());
        else{
            JsonObject object = new JsonObject();
            object.addProperty(Util.CUSTOMER_ID, customer.getCustomerID());
            object.addProperty(Util.NAME, customer.getCustomerName());
            object.addProperty(Util.EMAIL, customer.getEmail());
            object.addProperty(Util.PHONE, customer.getPhone());
            object.add(Util.ORDERS, getOrderFrom(customer.getOrderHistory()) );


            return buildResponseStatusOk200(object);
        }

    }


    private JsonArray getOrderFrom(ArrayList<Integer> list){
        JsonArray array = new JsonArray();
        Calendar cal = Calendar.getInstance();
        for(Integer integer : list){
            Order order =  orderBoundaryInterface.getOrderByID(integer);
            JsonObject object = new JsonObject();
            object.addProperty(Util.ID, order.getOrderID());
            cal.setTime(order.getOrderDate());
            object.addProperty(Util.ORDER_DATE, Util.dateToYYYYMMDDformat(cal));
            cal.setTime(order.getDeliveryDate());
            object.addProperty(Util.DELIVERY_DATE, Util.dateToYYYYMMDDformat(cal));
            object.addProperty(Util.AMOUNT, order.getTotalPrice());
            object.addProperty(Util.SURCHAGE, order.getSurcharge());
//            object.addProperty(Util.STATUS, order.getStatus());
            object.add(Util.STATUS, new JsonPrimitive(order.getStatus()));


            array.add(object);
        }

        return array;
    }




}
