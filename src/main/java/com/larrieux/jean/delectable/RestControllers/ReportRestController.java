package com.larrieux.jean.delectable.RestControllers;



import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.larrieux.jean.delectable.Entities.Report;
import com.larrieux.jean.delectable.Interfaces.ReportBoundaryInterface;
import com.larrieux.jean.delectable.Managers.ReportManager;
import com.larrieux.jean.delectable.Util;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;



/**
 * Created by Jeannius on 4/20/2016.
 */

@Path("/report")
public class ReportRestController extends AbstractController {


    private ReportBoundaryInterface reportBoundaryInterface = ReportManager.getInstance();



    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllReportTypes() {
        JsonArray array = new JsonArray();
        for(Report report : reportBoundaryInterface.getAllDeliveryReports()){
            JsonObject object = new JsonObject();
            object.addProperty(Util.ID, report.getId());
            object.addProperty(Util.NAME, report.getName());
            array.add(object);
        }

        return buildResponseStatusOk200(array);
    }



    @GET
    @Path("{rid}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getReportByID(@DefaultValue("") @QueryParam(Util.START_DATE) String startDate,
                                  @DefaultValue("") @QueryParam(Util.END_DATE) String endDate,
                                  @PathParam("rid") int rID) {

        if(rID!=0){
            if(rID!=reportBoundaryInterface.getReportIds()[2])return buildResponseStatusOk200(reportBoundaryInterface.getDeliveryReport(rID, startDate, endDate));
            else return buildResponseStatusOk200(reportBoundaryInterface.getRevenueReport(startDate,endDate));
        }
        return null;

    }


}
