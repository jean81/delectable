package com.larrieux.jean.delectable.RestControllers;



import com.google.gson.JsonObject;
import com.larrieux.jean.delectable.Entities.MenuItem;
import com.larrieux.jean.delectable.Entities.Order;
import com.larrieux.jean.delectable.Interfaces.MenuBoundaryInterface;
import com.larrieux.jean.delectable.Interfaces.OrderBoundaryInterface;
import com.larrieux.jean.delectable.Managers.MenuItemManager;
import com.larrieux.jean.delectable.Managers.OrderManager;
import com.larrieux.jean.delectable.Util;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.ArrayList;



/**
 * Created by Jeannius on 4/13/2016.
 */

@Path("/admin")
public class AdminRestController extends AbstractController {


    private OrderBoundaryInterface orderInterface = OrderManager.getInstance();
    private MenuBoundaryInterface menuInterface = MenuItemManager.getInstance();



    @PUT
    @Path("/menu")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addMenuItem(String json, @Context UriInfo uriInfo) {

        MenuItem item = getMenuItem(json);
        menuInterface.add(item);

        UriBuilder builder = uriInfo.getAbsolutePathBuilder();
        builder.path(Integer.toString(item.getMenuID()));

        JsonObject object = new JsonObject();
        object.addProperty(Util.ID, item.getMenuID());

        return buildResponseCreated201(object, builder);
    }



    private MenuItem getMenuItem(String json) {
        MenuItem item = new MenuItem();

        JsonObject jsonObject = getJsonObjectFromJsonString(json);
        String name = getStringFromJsonObject(jsonObject, Util.NAME);
        double price_per_person = getDoubleFromJsonObject(jsonObject, Util.PRICE_PER_PERSON);
        int minimum_order = getIntFromJsonObject(jsonObject, Util.MINIMUM_ORDER);
        ArrayList<String> categories = getArrayListFromJsonObject(jsonObject.getAsJsonArray(Util.CATEGORIES), Util.NAME);


        item.setName(name);
        item.setPrice_per_person(price_per_person);
        item.setMinimum_order(minimum_order);
        item.setCategories(categories);
        return item;
    }



    @POST
    @Path("/menu/{mid}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateMenuItemPrice(@PathParam("mid") int mID, String json) {
        MenuItem item = menuInterface.getMenuItemByID(mID);
        if (item == null) return buildResponseStatusNotFound404(mID, MenuItem.class.getSimpleName());
        else {
            JsonObject jsonObject = getJsonObjectFromJsonString(json);
            double price = getDoubleFromJsonObject(jsonObject, Util.PRICE_PER_PERSON);
            item.setPrice_per_person(price);
            return buildResponseNoContent204();
        }
    }



    @GET
    @Path("/surcharge")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSurcharge() {
        return buildResponseStatusOk200(orderInterface.getSurcharge());
    }



    @POST
    @Path("/surcharge")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateSurcharge(String json) {
        JsonObject jsonObject = getJsonObjectFromJsonString(json);
        double surcharge = getDoubleFromJsonObject(jsonObject, Util.SURCHAGE);

        orderInterface.setSurcharge(surcharge);
        return buildResponseNoContent204();
    }



    @POST
    @Path("/delivery/{oid}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response modifyOrderStatus(@PathParam("oid") int oid) {

        Order order = orderInterface.getOrderByID(oid);
        if (order == null) return buildResponseStatusNotFound404(oid, Order.class.getSimpleName());
        else {
            order.setStatus(OrderBoundaryInterface.DELIVERED);
            return buildResponseNoContent204();
        }

    }


}
