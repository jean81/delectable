package com.larrieux.jean.delectable.RestControllers;



import com.google.gson.*;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.util.ArrayList;



/**
 * Created by Jeannius on 4/13/2016.
 */
public class AbstractController {


    Gson gson = new GsonBuilder().setPrettyPrinting().create();



    public Response buildResponseStatusOk200(Object object) {
        String s = gson.toJson(object);
        return Response.status(Response.Status.OK).entity(s).build();
    }



    public Response buildResponseStatusNotFound404(int mID, String className) {
        return Response.status(Response.Status.NOT_FOUND).entity(className + " not found with ID: " + mID).build();
    }



    public Response buildResponseCreated201(Object object, UriBuilder builder) {
        String s = gson.toJson(object);
        return Response.created(builder.build()).entity(s).build();
    }



    public Response buildResponseNoContent204() {
        return Response.noContent().build();
    }


    public Response buildResponseBadReques400(Object object){
        String s = gson.toJson(object);
        return Response.status(Response.Status.BAD_REQUEST).entity(s).build();
    }

    public String getStringFromJsonObject(JsonObject jsonObject, String key) {
        return jsonObject.getAsJsonPrimitive(key).getAsString();
    }



    public double getDoubleFromJsonObject(JsonObject jsonObject, String key) {
        return jsonObject.getAsJsonPrimitive(key).getAsDouble();
    }



    public int getIntFromJsonObject(JsonObject jsonObject, String key) {
        return jsonObject.getAsJsonPrimitive(key).getAsInt();
    }



    public ArrayList<String> getArrayListFromJsonObject(JsonArray jsonArray, String key) {
        ArrayList<String> result = null;
        if (jsonArray != null) {
            result = new ArrayList<>();
            for (int i = 0; i < jsonArray.size(); i++) {
                JsonObject jsonObject2 = jsonArray.get(i).getAsJsonObject();
                result.add(getStringFromJsonObject(jsonObject2, key));
            }
        }

        return result;
    }



    public JsonObject getJsonObjectFromJsonString(String json) {
        JsonElement jsonElement = new JsonParser().parse(json);
        return jsonElement.getAsJsonObject();
    }


}

