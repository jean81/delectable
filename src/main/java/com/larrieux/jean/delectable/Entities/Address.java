package com.larrieux.jean.delectable.Entities;



/**
 * Created by Jlarrieux on 4/14/2016.
 */
public class Address {


    private int zipCode;
    private String streetLine, city, state;



    public Address(String streetName, String city, String state, int zipCode) {
        this.zipCode = zipCode;
        this.streetLine = streetName;
        this.city = city;
        this.state = state;
    }



    public int getZip() {
        return zipCode;
    }



    public String getStreet() {
        return streetLine;
    }



    public String getCity() {
        return city;
    }



    public String getState() {
        return state;
    }


}
