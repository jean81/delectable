package com.larrieux.jean.delectable.Entities;



import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;



/**
 * Created by Jeannius on 4/3/2016.
 */
public class MenuItem {


    static AtomicInteger atomicInteger = new AtomicInteger();
    private int menuID;
    private String name;
    private double price_per_person;
    private int minimum_order;
    private ArrayList<String> categories = new ArrayList<>();
    private Date dateCreated;
    private Date lastModifiedDate;



    public MenuItem() {

        menuID = atomicInteger.incrementAndGet();
        dateCreated = Calendar.getInstance().getTime();
        lastModifiedDate = Calendar.getInstance().getTime();
    }



    public int getMenuID() {
        return menuID;
    }



    public double getPrice() {
        return price_per_person;
    }



    public int getMinimumPeopleForOrder() {
        return minimum_order;
    }



    public void setPrice_per_person(double price_per_person) {
        this.price_per_person = price_per_person;
    }



    public void setMinimum_order(int minimum_order) {
        this.minimum_order = minimum_order;
    }



    public void setCategories(ArrayList<String> category) {
        this.categories = category;
    }



    public void setCategories(String category) {
        this.categories.add(category);
    }



    public String getName() {
        return this.name;
    }



    public void setName(String name) {
        this.name = name;
    }

    public ArrayList getCategories(){
        return  categories;
    }
}
