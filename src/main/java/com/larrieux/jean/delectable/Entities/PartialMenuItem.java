package com.larrieux.jean.delectable.Entities;



import java.util.ArrayList;



/**
 * Created by Jeannius on 4/23/2016.
 */
public class PartialMenuItem {

    private int menuID;
    private String name;
    private double price_per_person;
    private int minimum_order;
    private ArrayList<String> categories = new ArrayList<>();


    public void cloneMenu(MenuItem item){
        menuID = item.getMenuID();
        name = item.getName();
        price_per_person = item.getPrice();
        minimum_order = item.getMinimumPeopleForOrder();
        categories = item.getCategories();
    }
}
