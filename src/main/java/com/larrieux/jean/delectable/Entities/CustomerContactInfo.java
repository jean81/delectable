package com.larrieux.jean.delectable.Entities;



/**
 * Created by Jlarrieux on 4/14/2016.
 */
public class CustomerContactInfo {


    private String email, phone;



    public CustomerContactInfo(String email, String phone) {
        this.email = email;
        this.phone = phone;
    }



    public String getEmail() {
        return email;
    }



    public String getPhone() {
        return phone;
    }

}
