package com.larrieux.jean.delectable.Entities;



/**
 * Created by Jeannius on 4/20/2016.
 */
public abstract class Report {


    private int id;
    private String name;



    public Report(String name, int id) {
        this.name = name;
        this.id = id;
    }



    public int getId() {
        return id;
    }



    public String getName() {
        return name;
    }


}
