package com.larrieux.jean.delectable.Entities;



/**
 * Created by Jlarrieux on 4/14/2016.
 */
public class OrderItem {



    protected int count;
    private int menuID;
    private String name;



    public OrderItem(String name, int count, int menuItemId) {
        menuID = menuItemId;
        this.name = name;
        this.count = count;
    }



    public int getCount() {
        return count;
    }
}
