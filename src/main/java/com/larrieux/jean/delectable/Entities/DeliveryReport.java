package com.larrieux.jean.delectable.Entities;



import java.util.ArrayList;



/**
 * Created by Jeannius on 4/22/2016.
 */
public class DeliveryReport extends Report {





    private ArrayList<Order> orders = new ArrayList<>();



    public DeliveryReport(String name, int id) {
        super(name, id);
    }

    public void add(Order order){
        orders.add(order);
    }


    public void add(ArrayList<Order> orders){
        for(Order order : orders){
            this.orders.add(order);
        }
    }


    public int getOrderSize(){
        return orders.size();
    }






}
