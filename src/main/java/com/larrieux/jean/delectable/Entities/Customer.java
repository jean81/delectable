package com.larrieux.jean.delectable.Entities;



import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;



/**
 * Created by Jlarrieux on 4/14/2016.
 */
public class Customer {


    static AtomicInteger atomicInteger = new AtomicInteger();
    int customerID;
    CustomerContactInfo contactInfo;
    String customerName;
    private ArrayList<Integer> orderHistory = new ArrayList<>();



    public Customer(String name, CustomerContactInfo contact) {
        this.customerName = name;
        this.contactInfo = contact;
        this.customerID = atomicInteger.incrementAndGet();
    }



    public void addOrder(int order) {
        orderHistory.add(order);
    }



    public int getNumberOfOrders() {
        return orderHistory.size();
    }



    public ArrayList<Integer> getOrderHistory() {
        return orderHistory;
    }



    public int getCustomerID() {
        return customerID;
    }



    public String getCustomerName() {
        return customerName;

    }



    public String getEmail() {
        return contactInfo.getEmail();
    }



    public String getPhone() {
        return contactInfo.getPhone();
    }

}
