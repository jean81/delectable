package com.larrieux.jean.delectable.Entities;



import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;



/**
 * Created by Jeannius on 4/10/2016.
 */
public class Order {


    static AtomicInteger atomicInteger = new AtomicInteger();
    ArrayList<OrderItem> theOrder = new ArrayList<>();
    private Date deliveryDate, orderDate;
    private int customerID;
    private int orderID;
    private double totalPrice;
    private double surchage = 0;
    private String note;
    private String deliveryAddress;
    private String status;



    public Order() {
        orderID = atomicInteger.incrementAndGet();
    }



    public void add(OrderItem menuItem) {
        theOrder.add(menuItem);

    }



    public int getOrderID() {
        return orderID;
    }



    public Date getDeliveryDate() {
        return deliveryDate;
    }



    public void setDeliveryDate(Date date) {
        this.deliveryDate = date;
    }



    public String getDeliveryAddress() {
        return deliveryAddress;
    }



    public void setDeliveryAddress(String address) {
        this.deliveryAddress = address;
    }



    public String getNote() {
        return note;
    }



    public void setNote(String note) {
        this.note = note;
    }



    public int getCustomerID() {
        return customerID;
    }



    public void setCustomerID(int cid) {
        this.customerID = cid;
    }



    public Date getOrderDate() {
        return orderDate;
    }



    public void setOrderDate(Date time) {
        this.orderDate = time;
    }



    public ArrayList getOrderList() {
        return theOrder;
    }



    public double getTotalPrice() {
        return totalPrice;
    }



    public void setTotalPrice(double value) {
        this.totalPrice = value;
    }



    public double getSurcharge() {
        return surchage;
    }



    public void setSurcharge(double value) {
        this.surchage = value;
    }



    public String getStatus() {
        return status;
    }



    public void setStatus(String status) {
        this.status = status;
    }


}
