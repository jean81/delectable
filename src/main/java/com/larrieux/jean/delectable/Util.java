package com.larrieux.jean.delectable;



import java.util.Calendar;
import java.util.Date;



/**
 * Created by Jeannius on 4/15/2016.
 */
public class Util {


    public static final String SURCHAGE = "surcharge", NAME = "name", CATEGORIES = "categories",
            PRICE_PER_PERSON = "price_per_person", MINIMUM_ORDER = "minimum_order";
    public static final String DELIVERY_DATE = "delivery_date";
    public static final String DELIVERY_ADDRESS = "delivery_address";
    public static final String PERSONAL_INFO = "personal_info";
    public static final String EMAIL = "email";
    public static final String PHONE = "phone";
    public static final String NOTE = "note";
    public static final String ID = "id";
    public static final String COUNT = "count";
    public static final String ORDER_DETAIL = "order_detail";
    public static final String ORDER_DATE = "order_date";
    public static final String AMOUNT = "amount";
    public static final String STATUS = "status";
    public static final String ORDERED_BY = "ordered_by";
    public static final String DATE = "date";
    public static final String KEY = "key";
    public static final String START_DATE = "start_date";
    public static final String END_DATE = "end_date";
    public static final String CANCEL_URL = "cancel_url";
    public static final String ORDER_CANCEL = "/order/cancel/";
    public static final String CANNOT_CANCEL_ORDER_DUE_TODAY = "Cannot cancel order due today";
    public static final String ORDER_ID = "orderID";
    public static final String CUSTOMER_ID = "customerID";
    public static final String ORDERS = "orders";



    public static Date getDateFromString(String yyyymmdd) {

        int year = Integer.parseInt(yyyymmdd.substring(0, 4));
        int month = Integer.parseInt(yyyymmdd.substring(4, 6));
        int date = Integer.parseInt(yyyymmdd.substring(6, 8));

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, date);

        return cal.getTime();

    }



    public static Calendar calendarTimeToBeginningOFday(Calendar cal) {
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal;
    }



    public static String dateToYYYYMMDDformat(Calendar cal) {

        String year = String.valueOf(cal.get(Calendar.YEAR));

        String month = String.valueOf(cal.get(Calendar.MONTH));
        if (month.length() < 2) month = "0" + month;
        String date = String.valueOf(cal.get(Calendar.DATE));
        if (date.length() < 2) date = "0" + date;

        return year + month + date;
    }



    public static boolean isSameDate(Date date1, Date date2){
        Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();

        calendar1.setTime(date1);
        calendar2.setTime(date2);

        boolean sameYear = calendar1.get(Calendar.YEAR)==calendar2.get(Calendar.YEAR);
        boolean sameMonth = calendar1.get(Calendar.MONTH)==calendar2.get(Calendar.MONTH);
        boolean sameDate = calendar1.get(Calendar.DATE)==calendar2.get(Calendar.DATE);


        return  sameDate && sameMonth && sameYear;
    }


}
