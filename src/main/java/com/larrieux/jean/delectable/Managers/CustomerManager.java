package com.larrieux.jean.delectable.Managers;



import com.larrieux.jean.delectable.Entities.Customer;
import com.larrieux.jean.delectable.Interfaces.CustomerBoundaryInterface;

import java.util.ArrayList;



/**
 * Created by Jlarrieux on 4/14/2016.
 */
public class CustomerManager implements CustomerBoundaryInterface {


    private static CustomerManager customerManager = new CustomerManager();
    private static ArrayList<Customer> customerList = new ArrayList<>();



    private CustomerManager() {

    }



    public static CustomerManager getInstance() {
        return customerManager;
    }



    public int getCount() {

        return customerList.size();
    }



    @Override
    public void add(Customer john, int OrderID) {
        Customer tempCustomer = findByName(john.getCustomerName());
        if(tempCustomer==null){
            john.addOrder(OrderID);
            customerList.add(john);
        }
        else    tempCustomer.addOrder(OrderID);
    }



    @Override
    public boolean doesCustomerExist(Customer joe){
        Customer temp = findByName(joe.getCustomerName());
        if(temp==null) return false;
        else return true;
    }


    @Override
    public Customer getCustomerByID(int ID) {
        return findByID(ID);
    }



    @Override
    public ArrayList<Customer> getAllCustomers() {
        return customerList;
    }



    private Customer findByID(int id) {
        for (Customer customer : customerList) {
            if (customer.getCustomerID() == id) return customer;
        }

        return null;
    }


    @Override
    public Customer findByName(String name){
        for(Customer customer : customerList){
            if(customer.getCustomerName().equals(name)) return customer;

        }

        return null;
    }






}
