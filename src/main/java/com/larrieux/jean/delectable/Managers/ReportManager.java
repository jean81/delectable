package com.larrieux.jean.delectable.Managers;



import com.larrieux.jean.delectable.Entities.DeliveryReport;
import com.larrieux.jean.delectable.Entities.Order;
import com.larrieux.jean.delectable.Entities.Report;
import com.larrieux.jean.delectable.Entities.RevenueReport;
import com.larrieux.jean.delectable.Interfaces.OrderBoundaryInterface;
import com.larrieux.jean.delectable.Interfaces.ReportBoundaryInterface;
import com.larrieux.jean.delectable.Util;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;



/**
 * Created by Jlarrieux on 4/19/2016.
 */
public class ReportManager implements ReportBoundaryInterface {





    private static final int reportIDs[] = {801, 802, 803, 804};
    private static final String reportNames[] = {"Orders to deliver today", "Orders to deliver tomorrow", "Revenue Report", "Orders delivery report"};
    private static ReportManager reportManager = new ReportManager();
    OrderManager orderManager = OrderManager.getInstance();
    DecimalFormat decimalFormat = new DecimalFormat("#,###,###,###,###,###,###,##0.00");



    private ReportManager() {

    }



    public static ReportManager getInstance() {
        return reportManager;
    }



    public DeliveryReport getTodayDeliveryReport() {
        Calendar today = Calendar.getInstance();
        Calendar tomorrow = Calendar.getInstance();


        today = Util.calendarTimeToBeginningOFday(today);
        tomorrow = Util.calendarTimeToBeginningOFday(tomorrow);
        tomorrow.set(Calendar.DATE, tomorrow.get(Calendar.DATE) + 1);

        DeliveryReport todayReport = new DeliveryReport(reportNames[0], reportIDs[0]);
        todayReport.add(getOrderByDate(today.getTime(), tomorrow.getTime()));

        return todayReport;
    }




    public DeliveryReport getTomorrowDeliveryReport() {
        Calendar tomorrow = Calendar.getInstance();
        Calendar dayAfterTomorrow = Calendar.getInstance();

        tomorrow.set(Calendar.DATE, tomorrow.get(Calendar.DATE) + 1);
        tomorrow = Util.calendarTimeToBeginningOFday(tomorrow);
        dayAfterTomorrow.set(Calendar.DATE, dayAfterTomorrow.get(Calendar.DATE) + 2);
        dayAfterTomorrow = Util.calendarTimeToBeginningOFday(dayAfterTomorrow);

        DeliveryReport tomorrowReport = new DeliveryReport(reportNames[1], reportIDs[1]);
        tomorrowReport.add(getOrderByDate(tomorrow.getTime(),dayAfterTomorrow.getTime()));


        return tomorrowReport;
    }


    public DeliveryReport getSpecificDateDeliveryReport(String start, String end){
        Calendar startCal = Calendar.getInstance();
        startCal.setTime(Util.getDateFromString(start));
        startCal = Util.calendarTimeToBeginningOFday(startCal);

        Calendar endCal = Calendar.getInstance();
        endCal.setTime(Util.getDateFromString(end));
        endCal = Util.calendarTimeToBeginningOFday(endCal);

        DeliveryReport specificDateReport =new DeliveryReport(reportNames[3], reportIDs[3]);
        specificDateReport.add(getOrderByDate(startCal.getTime(), endCal.getTime()));

        return specificDateReport;

    }



    public ArrayList<Order> getOrderByDate(Date startDate, Date endDate) {
        Calendar startCalendar = Calendar.getInstance();
        Calendar endCalendar = Calendar.getInstance();
        startCalendar.setTime(startDate);
        endCalendar.setTime(endDate);
        ArrayList<Order> result = new ArrayList<>();
        for (Order order : orderManager.getOrderBook()) {
            if (order.getDeliveryDate().compareTo(startDate) >= 0 && order.getDeliveryDate().compareTo(endDate) < 0 && order.getStatus()!= OrderBoundaryInterface.CANCEL)
                result.add(order);
        }
        return result;
    }



    @Override
    public ArrayList<Report> getAllDeliveryReports() {
        Report report;
        ArrayList<Report> result = new ArrayList<>();
        for (int i = 0; i < reportIDs.length; i++) {
            report = new DeliveryReport(reportNames[i], reportIDs[i]);
            result.add(report);
        }

        return result;
    }



    @Override
    public DeliveryReport getDeliveryReport(int reportID, String startDate, String endDate) {

        if (reportID == reportIDs[0]) return getTodayDeliveryReport();
        else if (reportID == reportIDs[1]) return getTomorrowDeliveryReport();
        else return getSpecificDateDeliveryReport(startDate, endDate);

    }



    @Override
    public RevenueReport getRevenueReport(String startDate, String endDate) {
        RevenueReport report = new RevenueReport();
        Date start,end;
        if(!startDate.isEmpty() && !endDate.isEmpty()){
            start = Util.getDateFromString(startDate);
            end = Util.getDateFromString(endDate);
        }else {
            start= Util.getDateFromString("20000101");
            end =Util.getDateFromString("29990101") ;
        }

        ArrayList orders = getOrderByDate(start, end);
        report.setStart_date(startDate);
        report.setEnd_date(endDate);
        report.setOrders_placed(orders.size());
        report.setOrders_cancelled(getNumberOfOrderWithStatus(orders, "cancel"));
        report.setOrders_open(getNumberOfOrderWithStatus(orders, "open"));
        report.setFood_revenue(calculateFoodRevenue(orders));
        report.setSurcharge_revenue(calculateSurchargeRevenue(orders));
        return report;
    }



    @Override
    public int[] getReportIds() {
        return reportIDs;
    }



    private int getNumberOfOrderWithStatus(ArrayList<Order> orders, String status) {
        int result = 0;
        for (Order order : orders) {
            if (order.getStatus().equals(status)) result++;
        }

        return result;
    }




    private  double calculateFoodRevenue(ArrayList<Order> orders){
        double result=0;

        for(Order order : orders){
            if(order.getStatus()!= OrderBoundaryInterface.CANCEL)result += order.getTotalPrice();
        }
        return Double.parseDouble(decimalFormat.format(result));
    }


    private double calculateSurchargeRevenue(ArrayList<Order> orders){
        double result=0;
        for(Order order : orders){
            if(order.getStatus()!= OrderBoundaryInterface.CANCEL) result += order.getSurcharge();
        }
        return Double.parseDouble(decimalFormat.format(result));
    }





}
