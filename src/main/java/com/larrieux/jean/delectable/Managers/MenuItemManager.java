package com.larrieux.jean.delectable.Managers;



import com.larrieux.jean.delectable.Entities.MenuItem;
import com.larrieux.jean.delectable.Entities.PartialMenuItem;
import com.larrieux.jean.delectable.Interfaces.MenuBoundaryInterface;

import java.util.ArrayList;
import java.util.List;



/**
 * Created by Jeannius on 4/2/2016.
 */
public class MenuItemManager implements MenuBoundaryInterface {


    static MenuItemManager mainMenu = new MenuItemManager();
    private static List<MenuItem> theMenu = new ArrayList<>();



    private MenuItemManager() {

    }



    public static MenuItemManager getInstance() {
        return mainMenu;
    }



    public int getcount() {
        return theMenu.size();
    }



    @Override
    public void add(MenuItem item) {
        theMenu.add(item);

    }



    @Override
    public MenuItem getMenuItemByID(int id) {

        return (findByID(id));
    }



    @Override
    public List<MenuItem> getFullMenu() {
        ArrayList partialMenu = new ArrayList();
        for(MenuItem item : theMenu){
            PartialMenuItem p = new PartialMenuItem();
            p.cloneMenu(item);
            partialMenu.add(p);
        }
        return partialMenu;
    }



    private MenuItem findByID(int id) {
        for (int i = 0; i < theMenu.size(); i++) {
            MenuItem item = theMenu.get(i);
            if (item.getMenuID() == id) return item;
        }
        return null;
    }





}
