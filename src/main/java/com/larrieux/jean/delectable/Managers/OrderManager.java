package com.larrieux.jean.delectable.Managers;



import com.larrieux.jean.delectable.Entities.MenuItem;
import com.larrieux.jean.delectable.Entities.Order;
import com.larrieux.jean.delectable.Entities.OrderItem;
import com.larrieux.jean.delectable.Interfaces.OrderBoundaryInterface;
import com.larrieux.jean.delectable.Util;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;



/**
 * Created by Jeannius on 4/12/2016.
 */
public class OrderManager implements OrderBoundaryInterface {


    static ArrayList<Order> orderBook = new ArrayList<>();
    private static double surcharge;
    private static OrderManager orderManager = new OrderManager();
    double runningPrice;



    private OrderManager() {

    }



    public static OrderManager getInstance() {
        return orderManager;
    }



    @Override
    public void add(int menuItemID, int count, Order order) throws MinimumNumberOfPeopleNotMetForThisItem, itemNotFound {

        MenuItem menuItem = MenuItemManager.getInstance().getMenuItemByID(menuItemID);
        if(menuItem==null) throw new itemNotFound();
        else {

            if (isMinimumNumberOfPeopleConditionMet(menuItem, count)) {
                runningPrice += menuItem.getPrice() * count;
                order.add(new OrderItem(menuItem.getName(), count, menuItem.getMenuID()));
            } else throw new MinimumNumberOfPeopleNotMetForThisItem();
        }
    }



    @Override
    public ArrayList<Order> getOrderBook() {
        return orderBook;
    }



    @Override
    public Order getOrderByID(int oid) {
        return (findByID(oid));
    }



    @Override
    public double getSurcharge() {
        return surcharge;
    }



    @Override
    public void setSurcharge(double value) {
        surcharge = value;
    }



    @Override
    public void submitOrder(Order order) {
        order.setOrderDate(Calendar.getInstance().getTime());
        setFinalPrice(order);
        addSurchargeToOrder(order);
        order.setStatus("open");
        orderBook.add(order);

        runningPrice = 0;
    }



    @Override
    public void setDeliveryDate(Date deliveryDate, Order order) throws deliveryDateIsBeforeToday {
        Calendar today = Calendar.getInstance();
        today = Util.calendarTimeToBeginningOFday(today);
        Calendar deliveryCal = Calendar.getInstance();
        deliveryCal.setTime(deliveryDate);
        if(deliveryCal.compareTo(today)>=0) order.setDeliveryDate(deliveryDate);
        else  throw new deliveryDateIsBeforeToday();
    }



    private boolean isMinimumNumberOfPeopleConditionMet(MenuItem menuItem, int count) {
        return count >= menuItem.getMinimumPeopleForOrder();
    }



    public double getTotalPrice() {
        return runningPrice;
    }



    private Order findByID(int oid) {
        for (int i = 0; i < orderBook.size(); i++) {
            Order order = orderBook.get(i);
            if (order.getOrderID() == oid) return order;
        }

        return null;
    }



    public void addSurchargeToOrder(Order order) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(order.getDeliveryDate());
        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);

        if (dayOfWeek == 1 || dayOfWeek == 7) order.setSurcharge(getSurcharge());

    }



    private void setFinalPrice(Order order) {
        Date deliveryDate = order.getDeliveryDate();
        Calendar cal = Calendar.getInstance();
        cal.setTime(deliveryDate);
        DecimalFormat df = new DecimalFormat("#,###,###,###,###,###,###,##0.00");
        order.setTotalPrice(Double.parseDouble(df.format(runningPrice)));
    }





    public class MinimumNumberOfPeopleNotMetForThisItem extends RuntimeException {

    }


    public class itemNotFound extends RuntimeException{

    }





    public class deliveryDateIsBeforeToday extends RuntimeException {


    }
}
