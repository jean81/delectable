import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;



/**
 * Created by Jeannius on 4/22/2016.
 */
public class TestManagerParent {


    protected void clearStaticArrayList(Class<?> cl, String declaredFieldName) {
        try {
            Field list = cl.getDeclaredField(declaredFieldName);
            list.setAccessible(true);
            Object value = list.get(cl);
            Method method = value.getClass().getDeclaredMethod("clear", new Class[]{});
            method.invoke(value);


        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
