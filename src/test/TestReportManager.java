import com.larrieux.jean.delectable.Entities.DeliveryReport;
import com.larrieux.jean.delectable.Entities.MenuItem;
import com.larrieux.jean.delectable.Entities.Order;
import com.larrieux.jean.delectable.Entities.RevenueReport;
import com.larrieux.jean.delectable.Managers.MenuItemManager;
import com.larrieux.jean.delectable.Managers.OrderManager;
import com.larrieux.jean.delectable.Managers.ReportManager;
import com.larrieux.jean.delectable.Util;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;



/**
 * Created by Jeannius on 4/20/2016.
 */
public class TestReportManager extends TestManagerParent{

    ReportManager reportManager;
    OrderManager orderManager;
    MenuItemManager menuManager;
    MenuItem item1, item2;
    Order myOrder;

    @Before
    public void setUp() {
        reportManager = ReportManager.getInstance();
        orderManager = OrderManager.getInstance();
        menuManager = MenuItemManager.getInstance();
        item1 = createItem1();
        item2 = createItem2();
        menuManager.add(item1);
        menuManager.add(item2);
        myOrder = new Order();
    }

    @Test
    public void verifyThatThereAreFourReports() {
        assertTrue(reportManager.getAllDeliveryReports().size() >3);
    }



    @Test
    public void verifyGetOrderByDate() {

        myOrder.setDeliveryDate(Util.getDateFromString("20160328"));
        orderManager.submitOrder(myOrder);
        DeliveryReport report = reportManager.getSpecificDateDeliveryReport("20160322", "20160501");

        assertTrue(report.getOrderSize()>0);

    }



    @Test
    public void verifyOrderForToday() {

        myOrder.setDeliveryDate(Calendar.getInstance().getTime());
        orderManager.submitOrder(myOrder);
        assertTrue(reportManager.getTodayDeliveryReport().getOrderSize()>0);
    }



    @Test
    public void testOrderForTomorrow() {
        Calendar tomorrow = Calendar.getInstance();
        tomorrow.set(Calendar.DATE, tomorrow.get(Calendar.DATE)+1);
        myOrder.setDeliveryDate(tomorrow.getTime());
        orderManager.submitOrder(myOrder);
        assertTrue(reportManager.getTomorrowDeliveryReport().getOrderSize()>0);

    }

    private MenuItem createItem1(){
        MenuItem item1 = new MenuItem();
        item1.setName("lasagna");
        item1.setPrice_per_person(1.50);
        item1.setMinimum_order(3);
        return item1;
    }

    private MenuItem createItem2(){
        MenuItem item2 = new MenuItem();
        item2.setName("Mango");
        item2.setPrice_per_person(0.75);
        item2.setMinimum_order(1);
        return item2;
    }



    @Test
    public void testFoodRevenueInRevenueReport() {
        orderManager.add(item1.getMenuID(),10,myOrder);
        orderManager.add(item2.getMenuID(), 10, myOrder);
        myOrder.setDeliveryDate(Calendar.getInstance().getTime());
        orderManager.submitOrder(myOrder);
        String start = "20150101";
        String end = "20170101";

        double expectedFoodRevenue = item1.getPrice()*10 + item2.getPrice()*10;
        RevenueReport report = reportManager.getRevenueReport(start,end);
        assertEquals(expectedFoodRevenue,report.getFood_revenue(), 0);

    }



    @Test
    public void testSurchargeRevenueInRevenueReportIsZero() {
        orderManager.add(item1.getMenuID(),10,myOrder);
        orderManager.add(item2.getMenuID(), 10, myOrder);

        myOrder.setDeliveryDate(Calendar.getInstance().getTime());
        orderManager.submitOrder(myOrder);
        String start = "20150101";
        String end = "20170101";

        RevenueReport report = reportManager.getRevenueReport(start,end);
        assertEquals(0,report.getSurcharge_revenue(), 0);




    }



    @After
    public void tearDown() {
        Class<?> cl = orderManager.getClass();
        clearStaticArrayList(cl, "orderBook");

    }



}
