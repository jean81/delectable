import com.larrieux.jean.delectable.Entities.MenuItem;
import com.larrieux.jean.delectable.Managers.MenuItemManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;



/**
 * Created by Jeannius on 4/3/2016.
 */
public class TestMenuItemManager extends TestManagerParent{


    MenuItemManager menu;
    MenuItem item1;



    @Before
    public void setUp() {
        menu = MenuItemManager.getInstance();
        item1 = new MenuItem();

    }




    @Test
    public void addItemsToMenuAndShowItIsNotEmpty() {

        MenuItem item = new MenuItem();
        menu.add(item);
        assertEquals(1, menu.getcount());

    }



    @Test
    public void getMenuItemWithParticularID() {
        MenuItem item1 = new MenuItem();
        MenuItem item2 = new MenuItem();
        menu.add(item1);
        menu.add(item2);
        menu.getFullMenu();

        assertEquals(item1, menu.getMenuItemByID(item1.getMenuID()));
        assertEquals(item2, menu.getMenuItemByID(item2.getMenuID()));
    }







    @After
    public void tearDown() {
        Class<?> cl = menu.getClass();
        clearStaticArrayList(cl, "theMenu");
    }





}
