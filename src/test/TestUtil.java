import com.larrieux.jean.delectable.Util;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;



/**
 * Created by Jeannius on 4/17/2016.
 */
public class TestUtil {


    @Test
    public void testGetDateFromString() {

        String date1 = "20160417";

        Date myDate = Util.getDateFromString(date1);
        Calendar cal = Calendar.getInstance();
        cal.setTime(myDate);

        assertEquals(2016, cal.get(Calendar.YEAR));
        assertEquals(04, cal.get(Calendar.MONTH));
        assertEquals(17, cal.get(Calendar.DATE));

    }



    @Test
    public void testCalendarTimeBeginingOfDay() {
        Calendar calendar = Calendar.getInstance();
        calendar = Util.calendarTimeToBeginningOFday(calendar);
        assertEquals(0,calendar.get(Calendar.HOUR_OF_DAY));
        assertEquals(0,calendar.get(Calendar.MINUTE));
        assertEquals(0,calendar.get(Calendar.SECOND));
        assertEquals(0,calendar.get(Calendar.MILLISECOND));
    }


    @Test
    public void testTodayToYYYYMMDDformat() {
        Calendar cal = Calendar.getInstance();
        String year = String.valueOf(cal.get(Calendar.YEAR));
        String month = String.valueOf(cal.get(Calendar.MONTH));
        if(month.length()<2) month = "0"+month;
        String date = String.valueOf(cal.get(Calendar.DATE));
        if(date.length()<2)date = "0"+date;
        String YYYYMMDD = String.valueOf(year+month+date);

        assertEquals(YYYYMMDD, Util.dateToYYYYMMDDformat(cal));

    }



    @Test
    public void testDateIsSame() {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = (Calendar) cal1.clone();

        assertTrue(Util.isSameDate(cal1.getTime(),cal2.getTime()));

    }

}
