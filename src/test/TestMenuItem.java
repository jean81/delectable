import com.larrieux.jean.delectable.Entities.MenuItem;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;



/**
 * Created by Jeannius on 4/12/2016.
 */
public class TestMenuItem {


    MenuItem item;



    @Before
    public void setUp() {
        item = new MenuItem();

    }



    @Test
    public void menuItemHasPrice() {
        item.setPrice_per_person(125);
        assertEquals(125, item.getPrice(), 0);
    }



    @Test
    public void menuItemHasID() {
        assertTrue(item.getMenuID() > 0);

    }



    @Test
    public void twoMenuItemsHaveDifferentIds() {
        MenuItem item2 = new MenuItem();
        assertNotEquals(item.getMenuID(), item2.getMenuID());
    }


}
