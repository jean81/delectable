import com.larrieux.jean.delectable.Entities.Customer;
import com.larrieux.jean.delectable.Entities.CustomerContactInfo;
import com.larrieux.jean.delectable.Managers.CustomerManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;



/**
 * Created by Jeannius on 4/16/2016.
 */
public class TestCustomerManager extends TestManagerParent{

    CustomerManager customerManager;



    @Before
    public void setUp() {
        customerManager = CustomerManager.getInstance();
    }



    @Test
    public void customerListIsEmpty() {
        assertEquals(0, customerManager.getCount() );
    }



    @Test
    public void addCustomerAndVerifyListIsNotEmpty() {

        Customer john = new Customer("John Doe",new CustomerContactInfo("john@john", "111"));
        customerManager.add(john, 0);
        assertEquals(1, customerManager.getCount());
    }



    @Test
    public void testCustomerCanBeFoundByID() {
        Customer john = new Customer("John Doe",new CustomerContactInfo("john@john", "111"));
        customerManager.add(john, 0);
        assertEquals(john, customerManager.getCustomerByID(john.getCustomerID()) );
    }



    @Test
    public void testCustomerDoesNotExist() {
        CustomerContactInfo info = new CustomerContactInfo("joe@je", "1");
        Customer joe = new Customer("joe", info);
        assertFalse(customerManager.doesCustomerExist(joe));
    }



    @Test
    public void testCustomerDoesExist() {
        CustomerContactInfo info= new CustomerContactInfo("joed@joe", "1");
        Customer joe = new Customer("joe", info);
        customerManager.add(joe, 0);
        assertTrue(customerManager.doesCustomerExist(joe ));

    }



    @Test
    public void testFindCustomerByName() {
        CustomerContactInfo info= new CustomerContactInfo("joed@joe", "1");
        Customer joe = new Customer("joe", info);
        customerManager.add(joe, 0);
        assertEquals(joe, customerManager.findByName("joe"));
    }



    @After
    public void tearDown(){
        Class<?> cl = customerManager.getClass();
        clearStaticArrayList(cl, "customerList");

    }


}
