import com.larrieux.jean.delectable.Entities.Address;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;



/**
 * Created by Jeannius on 4/16/2016.
 */
public class TestAddress {


    public static final String STREET = "street";
    public static final String CITY = "city";
    public static final String STATE = "state";
    public static final int ZIP_CODE = 12345;
    Address address;


    @Before
    public void setUp() {
        address = new Address(STREET, CITY, STATE, ZIP_CODE);
    }


    @Test
    public void testZipCodeIsAssignedProperly() {

        assertEquals(address.getZip(), ZIP_CODE);
    }



    @Test
    public void testStreetAddressAssingnment() {

        assertEquals(address.getStreet(), STREET);

    }



    @Test
    public void testCityAddressAssignment() {
        assertEquals(address.getCity(), CITY);
    }



    @Test
    public void testStateAddressAssignment() {
        assertEquals(address.getState(), STATE);
    }


}
