import com.larrieux.jean.delectable.Entities.MenuItem;
import com.larrieux.jean.delectable.Entities.Order;
import com.larrieux.jean.delectable.Interfaces.MenuBoundaryInterface;
import com.larrieux.jean.delectable.Managers.MenuItemManager;
import com.larrieux.jean.delectable.Managers.OrderManager;
import com.larrieux.jean.delectable.Util;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;



/**
 * Created by Jeannius on 4/12/2016.
 */
public class TestOrderManager extends TestManagerParent {


    OrderManager om;
    MenuItem item1 = new MenuItem();
    MenuItemManager menuItemManager = MenuItemManager.getInstance();


    @Before
    public void setUp() {
        om = OrderManager.getInstance();
    }



    @Test
    public void addItemToOrderWithNoException() {
        Order order = new Order();
        item1.setMinimum_order(1);
        menuItemManager.add(item1);
        om.add(item1.getMenuID(), 3, order);


    }



    @Test(expected = OrderManager.MinimumNumberOfPeopleNotMetForThisItem.class)
    public void addItemToOrderWithLessThanMinimumNumberOfPeopleForOrder() {
        Order order = new Order();
        item1.setMinimum_order(5);
        menuItemManager.add(item1);
        om.add(item1.getMenuID(), 3, order);
    }



    @Test
    public void verifyOrderSurchargeNotSet() {
        Order order = new Order();
        Calendar calendar = Calendar.getInstance();
        Date date = Util.getDateFromString("20160419");
        calendar.setTime(date);
        order.setDeliveryDate(calendar.getTime());

        om.addSurchargeToOrder(order);
        assertEquals(order.getSurcharge(), 0, 0);
    }



    @Test
    public void verifyOrderSurchargeIsSet() {
        Order order = new Order();
        Calendar calendar = Calendar.getInstance();
        Date date = Util.getDateFromString("20160323");
        calendar.setTime(date);
        order.setDeliveryDate(calendar.getTime());
        om.setSurcharge(15);

        om.addSurchargeToOrder(order);
        assertEquals(order.getSurcharge(), om.getSurcharge(), 0);

    }



    @Test
    public void verifyRunningPrice() {

        Order order = new Order();
        MenuItem item =new MenuItem();
        item.setName("Fountain of Youth");
        item.setPrice_per_person(2.49);
        MenuBoundaryInterface menuManager = MenuItemManager.getInstance();
        menuManager.add(item);
        om.add(item.getMenuID(),10,order);

        assertEquals(24.9, om.getTotalPrice(), 0.001);

    }



    @Test
    public void verifyOrderCanBeFoundByID() {

        Order order1 = new Order();
        Calendar cal1 = Calendar.getInstance();
        order1.setDeliveryDate(cal1.getTime());
        Order order2 = new Order();
        order2.setDeliveryDate(cal1.getTime());
        om.submitOrder(order1);
        om.submitOrder(order2);

        assertEquals(order1, om.getOrderByID(order1.getOrderID()));
        assertEquals(order2, om.getOrderByID(order2.getOrderID()));

    }



    @After
    public void tearDown() {
        Class<?> cl = om.getClass();
        om.setSurcharge(0);
        clearStaticArrayList(cl, "orderBook");
    }


}
