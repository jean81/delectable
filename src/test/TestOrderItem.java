import com.larrieux.jean.delectable.Entities.OrderItem;
import org.junit.Test;

import static org.junit.Assert.assertEquals;



/**
 * Created by Jlarrieux on 4/14/2016.
 */
public class TestOrderItem {


    OrderItem orderItem;



    @Test
    public void orderItemHasCount() {
        orderItem = new OrderItem("test", 5,0);
        assertEquals(5, orderItem.getCount());

    }

}
