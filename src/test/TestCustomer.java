import com.larrieux.jean.delectable.Entities.Customer;
import com.larrieux.jean.delectable.Entities.CustomerContactInfo;
import com.larrieux.jean.delectable.Entities.Order;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;



/**
 * Created by Jlarrieux on 4/14/2016.
 */
public class TestCustomer {


    public static final String J_LAR = "J LAR";
    public static final String EMAIL = "j@j";
    public static final String PHONE = "111-111-1111";
    Customer customer;



    @Before
    public void setUp() {
        CustomerContactInfo info = new CustomerContactInfo(EMAIL, PHONE);
        customer = new Customer(J_LAR, info);
    }



    @Test
    public void testNoOrderInOrderHistory() {
        assertEquals(0, customer.getNumberOfOrders());

    }



    @Test
    public void testNonEmptyOrderHistory() {

        Order order = new Order();
        customer.addOrder(order.getOrderID());
        assertEquals(customer.getOrderHistory().size(), 1);

    }


    @Test
    public void testCustomerName() {
        assertEquals(customer.getCustomerName(), J_LAR);
    }



    @Test
    public void testCustomerEmail() {
        assertEquals(customer.getEmail(), EMAIL);
    }



    @Test
    public void testCustomerPhone() {
        assertEquals(customer.getPhone(), PHONE);

    }

}
