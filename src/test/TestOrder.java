import com.larrieux.jean.delectable.Entities.Order;
import com.larrieux.jean.delectable.Entities.OrderItem;
import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;

import static org.junit.Assert.assertEquals;



/**
 * Created by Jeannius on 4/10/2016.
 */
public class TestOrder {


    public static final String ADDRESS = "address";
    public static final String NOTE = "NOTE";
    public static final int CUSTOMER_ID = 5;
    public static final double TOTAL_PRICE = 150.99;
    public static final int SURCHARGE = 15;
    public static final String STATUS = "Open";
    Order myOrder;



    @Before
    public void setUp() {
        myOrder = new Order();
    }



    @Test
    public void testEmptyOrderList() {
        assertEquals(0, myOrder.getOrderList().size());

    }



    @Test
    public void testNonEmptyOrderList() {
        OrderItem orderItem = new OrderItem("name", 5, 0);
        myOrder.add(orderItem);
        assertEquals(1, myOrder.getOrderList().size());

    }



    @Test
    public void testDateWasSetCorrectly() {

        Calendar cal = Calendar.getInstance();
        myOrder.setDeliveryDate(cal.getTime());

        assertEquals(cal.getTime(), myOrder.getDeliveryDate());
    }



    @Test
    public void testDeliveryAddressWasSetUpCorrectly() {
        myOrder.setDeliveryAddress(ADDRESS);
        assertEquals(ADDRESS, myOrder.getDeliveryAddress());
    }



    @Test
    public void testNoteWasSetUpCorrectly() {
        myOrder.setNote(NOTE);
        assertEquals(NOTE, myOrder.getNote());
    }



    @Test
    public void testCustomerIDWasSetUpCorrectly() {
        myOrder.setCustomerID(CUSTOMER_ID);
        assertEquals(CUSTOMER_ID, myOrder.getCustomerID());
    }



    @Test
    public void testTotalPrice() {
        myOrder.setTotalPrice(TOTAL_PRICE);
        assertEquals(TOTAL_PRICE, myOrder.getTotalPrice(),0);

    }



    @Test
    public void testSurchargeWasSetUpCorrectly() {

        assertEquals(myOrder.getSurcharge(), 0, 0);
        myOrder.setSurcharge(SURCHARGE);
        assertEquals(myOrder.getSurcharge(),SURCHARGE,0);

    }



    @Test
    public void testStatus() {

        myOrder.setStatus(STATUS);
        assertEquals(myOrder.getStatus(), STATUS);

    }

}
